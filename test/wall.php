<?

header('Content-Type: text/html; charset=utf-8');
include_once dirname(dirname(__FILE__)) . "/vk_api.php";

function pre_print($data) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

$vk = new Holy_VKontakteAPI();
if ($vk->OK()) {
    $user_id = $vk->GetUserIDByName("nubilius");
    $wall = $vk->GetWallComments(5797324);
    pre_print($wall);
} else {
    $errors = $vk->GetErrors();
    pre_print($errors);
};