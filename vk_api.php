<?php
class Holy_VKontakteAPI {

    protected $errors = array();
    protected $users = array();
    protected $ok = true;

    /**
     * Сообщает, нормально ли прошла иницализация.
     * 
     * @return bool
     */
    public function OK() {
        return $this->ok;
    }

    public function VKontakteAPI() {
        if (!function_exists("curl_init")) {
            $this->ok = false;
            $this->errors[] = "У вас отсутствует библиотека CURL. Очень жаль :(";
            return false;
        }
    }

    /**
     * Возвращает список имен+фамилий пользователей, где ключи - id пользователя.
     * 
     * @return array
     */
    public function GetUsersArray() {
        return $this->users;
    }

    /**
     * Возвращает массив ошибок, каждый элемент - текст ошибки.
     * 
     * @return array
     */
    public function GetErrors() {
        return $this->errors;
    }

    protected function _Curl($url) {
        $result = false;
        if (function_exists("curl_init")) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 90);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            $result = curl_exec($ch);
            if ($result==false){
                $this->errors[]=curl_error($ch);
            }
            curl_close($ch);
        }else{
            $this->errors[]="не подключег модуль curl.";
        }

        return $result;
    }

    protected function _Defaults_Wall() {
        return array("offset" => 0,
            "count" => 10,
            "extended" => "0",
            "filter" => "all");
    }

    
    /**
     * Возвращает id пользователя по коду-имени, или false, если не найден
     * 
     * @param string $name код пользователя
     * @return int/bool
     */
    public function GetUserIDByName($name){
        $name=filter_var($name, FILTER_SANITIZE_STRING);
        $_new_user = 0;
        $_users_src = $this->_VK_Method("users.get", Array(
                "uids" => $name,
                    ));
        if (isset($_users_src[0]['uid'])){
            $_new_user=$_users_src[0]['uid'];
        }
        return $_new_user;
    }

    /**
     * Возвращает массив элементов.
     * 
     * @param array $users_id массив id пользователей
     * @return array массив пользователей
     */
    public function GetUsers($users_id) {
        $_new_user = false;
        if (count($users_id)) {
            $users_url = implode(",", $users_id);
            $_users_src = $this->_VK_Method("users.get", Array(
                "uids" => $users_url,
                    ));
            if (is_array($_users_src))
            foreach ($_users_src as $_user) {
                $_new_user[$_user['uid']] = $_user['first_name'] . " " . $_user['last_name'];
            }
        };
        return $_new_user;
    }

    protected function _VK_Method($method, $params) {
        $url = "https://api.vk.com/method/" . $method . "?";

        $cnt = 0;
        foreach ($params as $_key => $_param) {
            $cnt++;
            if ($cnt > 1)
                $url.="&";
            $url.=$_key . "=" . $_param;
        };

        $data_src = $this->_Curl($url);
        $data = json_decode($data_src, true);
        if (isset($data['error'])) {
            $this->errors[] = $data['error']['error_msg'];
            $data = false;
        } else {
            if (isset($data['response']))
                $data = $data['response'];
        }
        return $data;
    }

    /**
     * Возвращает массив комментариев со стены пользователя.
     * 
     * @param type $user_id уникальный номер одного пользователя
     * @param array $data <p>Массив необязательных дополнительных параметров: <br>
     * <b>offset</b> - смещение в списке записей на стене<br>
     * <b>count</b> - число записей на стене<br>
     * <b>extended</b> - расширенный формат записей<br>
     * <p><b>filter</b> - фильтрация комментариев (
     * <b>owner</b> - сообщения на стене от ее владельца, 
     * <b>others</b> - сообщения на стене не от ее владельца, 
     * <b>all</b> - все сообщения на стене (значение пол умолчанию)
     * </p>
     * @return array массив параметров.
     */
    public function GetWallComments($user_id, $data = array()) {
        $count = intval($count);
        if ($count <= 0)
            $count = 1;
        $comments = false;

        $data['owner_id'] = $user_id;
        $f_data = array_merge($this->_Defaults_Wall(), $data);

        $comments = $this->_VK_Method("wall.get", $f_data);
        $comments_out = $comments;

        //получаем список пользователей
        $users_id = array();
        if ($comments_out)
        foreach ($comments_out as $_comment) {
            if (isset($_comment['from_id'])) {
                $users_id[] = $_comment['from_id'];
            }
        };
        $this->users = $this->GetUsers($users_id);

        return $comments_out;
    }

}

?>